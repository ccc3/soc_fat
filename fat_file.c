#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include "fat_fs_spec.h"
#include "fat_tools.h"
#include "fat_file.h"
#include "fat_file_tools.h"
#include "fat_dir_table.h"
#include "utils.h"

#define SIZE_OF_SECTOR                 512  //  MyFAt.sector_size
#define SIZE_OF_DIRECTORY_ENTRY        32
#define CLUSTER_OFFSET                 2

int g_open(const char* pathname,int flags)
{
   unsigned char tmp[MAX_PATH];
   int nb_read                   = 0;
   unsigned int  cluster_number  = MyFAt.root_cluster;
   unsigned short int fd		   =0;

   if ( Des_init == 0)
   {
      printf("init\n\n");
      memset(File_Descriptor,0,sizeof(Descriptor)*NB_FD); 
      Des_init=1;
   }

   while(read_path(pathname, tmp, &nb_read))
   {
      if(cluster_number == -1)
         return -1;
      cluster_number=read_table_entry(cluster_number,tmp);
   }
   if(cluster_number!=-1)
   {
      while( File_Descriptor[fd].used != 0 && fd < 32 )
      {
         fd++;
      }

      if(fd<NB_FD)
      {
         File_Descriptor[fd].first_cluster_num=cluster_number;
         File_Descriptor[fd].used=1;
      }
   }else
      return -1;

   return fd;     
}

size_t g_read(int fd, void * buffer, size_t count)
{
   unsigned int block = 0;


   if (fd < 0 || fd >= MAX_NB_FD)
      return -1;
   if (buffer == NULL )
      return -1;
   if (count < 1)
      return -1;
   if ( File_Descriptor[fd].used !=1)
      return -1;
   if(  !File_Descriptor[fd].offset )
      block = lba_to_block(cluster_to_lba(File_Descriptor[fd].first_cluster_num));
   else
      block =


   return NULL;
}
















