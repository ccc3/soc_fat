#include <stdio.h>
#include <stdlib.h>

#include "fat_fs_spec.h"
#include "fat_dir_table.h"
#include "utils.h"


/*read size octet at offset offset from buffer and return it as little or big indian if set to 1 or 0 */
int read_entry(int offset, int size, unsigned char * buffer,int little_indian)
{
   int turn = size;
   unsigned int res = 0;
   unsigned int mask = 0x000000ff;
   if(little_indian)
   {
      while( turn != 1 )
      {
         res = res | (buffer[offset + (turn-1)] & mask);
         res = res << 8;
         turn--;
      }
      res = (buffer[offset + (turn-1)] & mask) | res;
   }
   else
   {
      turn=0;
      while( turn != size - 1 )
      {

         res = res  | (buffer[ offset + turn ] & mask );
         res = res << 8;
         turn++;
      }
      res = res | (buffer[offset + turn] & mask);
   }
   return res;
}

int read_path(const char * pathname,unsigned char * buffer,int * nb_read  )
{
   if (pathname[*nb_read] == '\0')
      return 0;
   int i = (pathname[*nb_read] == '/')? (*nb_read) + 1 : *nb_read;
   int j = 0;
   while(pathname[i] != '/' && pathname[i] != '\0')
   {
      buffer[j]=pathname[i];    
      j++;
      i++;
   }
   buffer[j]='\0';
   *nb_read += j+1;
   return 1;

}

int fat_init( Fatfs* fs)
{

   FILE *f;
   short int i=0;
   short int off=0;
   unsigned char buffer[513]={0};
   /**************************IOC_READ***************************/
   if((f=fopen(HD_FAT,"r")) == NULL )
   {
      perror("open");
      exit(-1);
   }
   fread(buffer,1,512,f);
   /**************************************************************/
   fs->sector_size         = read_entry(BPB_BYTSPERSEC, buffer,1);
   fs->sector_per_cluster  = read_entry(BPB_SECPERCLUS, buffer,1);
   fs->cluster_size        = fs->sector_size*fs->sector_per_cluster;
   fs->rsvd_sector_cnt     = read_entry(BPB_RSVDSECCNT,buffer,1);
   fs->num_fats            = read_entry(BPB_NUMFATS,buffer,1);
   fs->fat_size            = read_entry(BPB_FAT32_FATSZ32,buffer,1);
   fs->fs_info_sector      = read_entry(BPB_FAT32_FSINFO,buffer,1);
   fs->total_sector        = read_entry(BPB_TOTSEC32, buffer,1);
   fs->fat_start           = fs->rsvd_sector_cnt*fs->sector_size;
   fs->data_start          = (fs->rsvd_sector_cnt +fs->num_fats*fs->fat_size)*fs->sector_size;
   fs->fat_type            =  32;                                                                         
   /* fs->fat_type            = ((fs->total_sector/fs->sector_per_cluster) > FAT32_MIN_NB_CLUSTER)?32:\
      (((fs->total_sector/fs->sector_per_cluster) < FAT16_MIN_NB_CLUSTER)?12:16); */
   fs->rootEntCnt          = read_entry(BPB_ROOTENTCNT,buffer,1);
   fs->signature           =read_entry(SIGNATURE_POSITION,buffer,0);
   //fs->next_free_cluster a faire plus tard   
   fs->root_cluster=read_entry(BPB_FAT32_ROOTCLUS,buffer,1);

   fseek(f,fs->fat_start,SEEK_SET);
   fread(buffer,1,512,f);
   fs->fat_table_page=1;
   while(i<128)
   {
      fs->fat_table[i]=read_entry( off, CLUSTER_SIZE, buffer, 1);
      off+=4;
      i++;
   }

   fclose(f); 

   return 0;
}

void print_fat(Fatfs * fs)
{
   printf("################################ FAT SPEC ###############################\n");  
   printf("                                    \t\t     HEX DEC \n");         
   COLOR_ON;
   printf("Sector size                         \t\t%8x|%d   ",fs->sector_size,           fs->sector_size ); 
   COLOR_OFF;
   printf("Sector per cluster                  \t\t%8x|%d \n",fs->sector_per_cluster,    fs->sector_per_cluster ); 
   COLOR_ON;
   printf("Size of cluster                     \t\t%8x|%d   ",fs->cluster_size,       fs->cluster_size ); 
   COLOR_OFF;
   printf("Reserved Sector count               \t\t%8x|%d  \n",fs->rsvd_sector_cnt,       fs->rsvd_sector_cnt ); 
   COLOR_ON;
   printf("Number of FATs                      \t\t%8x|%d ",fs->num_fats,              fs->num_fats ); 
   COLOR_OFF;
   printf("Size of one FAT                     \t\t%8x|%d \n",fs->fat_size,              fs->fat_size ); 
   COLOR_ON;
   printf("Sector number of Fs Info sector     \t\t%8x|%d ",fs->fs_info_sector,        fs->fs_info_sector ); 
   COLOR_OFF;
   printf("Total numbers of sector             \t\t%8x|%d \n",fs->total_sector,          fs->total_sector ); 
   COLOR_ON;
   printf("Beginning of FAT region             \t\t%8x|%d ",fs->fat_start,             fs->fat_start ); 
   COLOR_OFF;
   printf("Beginning of Data region            \t\t%8x|%d \n",fs->data_start,            fs->data_start ); 
   COLOR_ON;
   printf("Type of FAT                         \t\t%8x|%d ",fs->fat_type,              fs->fat_type ); 
   COLOR_OFF;
   printf("Signature                           \t\t%8x|%d \n ",fs->signature,             fs->signature ); 
}

