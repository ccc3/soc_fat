#ifndef _FILE_H
#define _FILE_H

#define MAX_PATH 255
#define MAX_NB_FD    6

//init
static int Des_init = 0;

typedef struct
{
   unsigned int flag                      ;     // mode
   char path[MAX_PATH]                    ;    // path
   unsigned char used                     ;    // Descriptor used
   unsigned int offset                    ;    // current offset
   unsigned int first_cluster_num         ;    // cluster @
   unsigned int size_file                 ;    // size of file
}Descriptor;

Descriptor File_Descriptor[MAX_NB_FD];   

int g_open(const char *pathname, int flags);
size_t g_read(int fd, void *buffer, size_t count);

#endif
