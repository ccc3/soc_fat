#ifndef _FAT_DIR_TABLE
#define _FAT_DIR_TABLE
/*this is for fat32
The  Directory Table of the Data region have 2 types of directory entry short and long.
-Short consist of 8 characters maximum including the dot. Over 8 characteres it is considered as long directory entries.
Long directories entry are a succesion of long directories structure contiguousely stored till the last structure wich is a short direcotry structure.
*/ 


//Short Directory Entry Structure  names and offset of the data stored in the 32 bytes field.
//                          offset | length
#define DIR_ORD                 0  , 1
#define DIR_NAME                0  , 11     
#define DIR_ATTR                11 , 1            
#define DIR_NTRES               12 , 1
#define DIR_CRT_TIMES_TENTH     13 , 1
#define DIR_FST_CLUS_HI         20 , 2
#define DIR_WRT_TIME            22 , 2
#define DIR_WRT_DATE            24 , 2 
#define DIR_FST_CLUS_LO         26 , 2
#define DIR_FILE_SIZE           28 , 4

/*********************************************************************************************************/

//Long Directory Entry Structure  names and offset of the data stored in the 32 bytes field.
//                      offset | length
#define LDIR_ORD            0  , 1         //from 0x01 to 0x0f  this allow 255 character maximum, 0x4? is the last long entry marker              
#define LDIR_NAME_1         1  , 10
#define LDIR_ATTR           11 , 1
#define LDIR_TYPE           12 , 1
#define LDIR_CHK_SUM        13 , 1
#define LDIR_NAME_2         14 , 12
#define LDIR_FST_CLUS_HI    26 , 2
#define LDIR_NAME_3         28 , 4

/*********************************************************************************************************/

//LDIR_ATTR  attribute  
#define ATTR_READ_ONLY          0x01
#define ATTR_HIDDEN             0x02
#define ATTR_SYSTEM             0x04
#define ATTR_VOLUME_ID          0x08
#define ATTR_DIRECTORY          0x10
#define ATTR_ARCHIVE            0x20
#define ATTR_LONG_NAME_MASK     0x0f  //ATTR_READ_ONLY|ATTR_HIDDEN|ATTR_SYSTEM|ATTR_VOLUME_ID

/*******************************************************************************************************/

#endif

