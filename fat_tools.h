#ifndef _FAT_TOOLS_H
#define _FAT_TOOLS_H

#include "fat_fs_spec.h"


/*read an entry in the table at the offset offset, of size sizr from the buffer buffer and invert it if little_indian > 0
return the entry read */ 
int read_entry(int offset, int size,unsigned char * buffer,int little_indian);

/*print the fat spec contained in Fatfs */
void print_fat(Fatfs * fs);

int fat_init( Fatfs* fs);

int read_path(const char * pathname,unsigned char * buffer,int * nb_read  );






#endif
