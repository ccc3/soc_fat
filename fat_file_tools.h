#ifndef _FAT_FILE_TOOLS_H
#define _FAT_FILE_TOOLS_H

unsigned int lba_to_cluster(unsigned int  lba);
unsigned int cluster_to_lba(unsigned int cluster_number);
unsigned int lba_to_block(unsigned int lba);
int get_next_cluster(unsigned int cluster_number);
unsigned int read_cluster(unsigned char * short_buf);

int read_block( unsigned char * block,unsigned  char * buf,unsigned int size_elem);

int get_name_from_short(unsigned char * dir_entry,unsigned char * entry_name);
int get_name_from_long(unsigned char *dir_entry, unsigned char *entry_name);
int fat_strcmp(unsigned char* filename ,unsigned char *entry_name); 

int  read_table_entry(unsigned int cluster_number,unsigned char* file_name);

unsigned char illegal_short(unsigned char c);
int is_short(unsigned char * string, unsigned char * sc_string);
unsigned char to_upper(unsigned char c);

#endif 
