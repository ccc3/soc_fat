#ifndef _UTILS
#define _UTILS
#define HD_FAT "hd.bin"

#define COLOR_ON   printf("%c[%d;%d;%dm",27,1,30,47)
#define COLOR_OFF  printf("%c[%dm\n",27,0)

int get_offset(int off,int length);
int get_length(int off, int length);

#endif

