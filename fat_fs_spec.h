#ifndef FAT_SPEC_H
#define FAT_SPEC_H


//Name and offset of data stored on sector 0 of a fat file  system //
//BPB=Bios parameter block

// Boot Sector            offset |  length
#define BS_JMPBOOT            0  ,  3
#define BS_OEMNAME            3  ,  8
#define BPB_BYTSPERSEC        11 ,  2
#define BPB_SECPERCLUS        13 ,  1
#define BPB_RSVDSECCNT        14 ,  2
#define BPB_NUMFATS           16 ,  1
#define BPB_ROOTENTCNT        17 ,  2
#define BPB_TOTSEC16          19 ,  2
#define BPB_MEDIA             21 ,  1
#define BPB_FATSZ16           22 ,  2
#define BPB_SECPERTRK         24 ,  2
#define BPB_NUMHEADS          26 ,  2
#define BPB_HIDDSEC           28 ,  4
#define BPB_TOTSEC32          32 ,  4

// FAT 12/16
#define BS_FAT_DRVNUM         36 , 1
#define BS_FAT_BOOTSIG        38 , 1
#define BS_FAT_VOLID          39 , 4
#define BS_FAT_VOLLAB         43 , 11
#define BS_FAT_FILSYSTYPE     54 , 8

// FAT 32
#define BPB_FAT32_FATSZ32     36 , 4
#define BPB_FAT32_EXTFLAGS    40 , 2
#define BPB_FAT32_FSVER       42 , 2
#define BPB_FAT32_ROOTCLUS    44 , 4
#define BPB_FAT32_FSINFO      48 , 2
#define BPB_FAT32_BKBOOTSEC   50 , 2
#define BS_FAT32_DRVNUM       64 , 1
#define BS_FAT32_BOOTSIG      66 , 1
#define BS_FAT32_VOLID        67 , 4
#define BS_FAT32_VOLLAB       71 , 11
#define BS_FAT32_FILSYSTYPE   82 , 8

//**************End Boot Sector******************//


#define PARTITION_BEGIN_LBA      454 , 4
#define PARTION_SIZE             458 	 
#define SIGNATURE_POSITION       510 , 2
#define SIGNATURE_VALUE          0xAA55  

#define CLUSTER_SIZE             32       //28 in fact



/* Fat Determination  /!\ this is the only way to REALLY determine what type of fat that you are using  */
#define FAT32_MIN_NB_CLUSTER  65525+1
#define FAT16_MAX_NB_CLUSTER  65525
#define FAT16_MIN_NB_CLUSTER  4085      
#define FAT12_MAX_NB_CLUSTER  4085-1
/********************************************************************************************************/



/*************This struct contains all the info needed in order to read a fat 32 volume******************/

#define SIZE_CLUSTER_BUF	128

typedef struct _Fatfs
{
unsigned short  sector_size;		//power of 2
unsigned char   sector_per_cluster;	// power of 2 greater than 2^2
unsigned int  cluster_size;
unsigned char   rsvd_sector_cnt;	// 32 on fat32
unsigned char   num_fats;		// usually 2
unsigned int  fat_size;    // nb sectors occupied by one fat
unsigned char   rootEntCnt;		// 0 on fat 32
unsigned char   fs_info_sector;         // usually sector <1>
unsigned int  total_sector;		// totsec32 for fat32 totsec16 for fat12/16
//unsigned int  next_free_cluster;      // info on the fsInfo block usually sector <1>
unsigned int  fat_start;		// begining of the fat region
unsigned int  data_start;		// begining of the data region (for fat 32 also root)
unsigned int  root_cluster;     
unsigned char fat_type;			//32, 16 or 12
unsigned int partion_start;		//lba of the begining of the partiton if there is
unsigned int fat_table[SIZE_CLUSTER_BUF];     //
unsigned int fat_table_page;
unsigned short signature;
}Fatfs;
/******************************************************************************************************/

Fatfs MyFAt;

#endif

