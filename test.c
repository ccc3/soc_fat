#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include "utils.h"
#include "fat_fs_spec.h"
#include "fat_tools.h"
#include "fat_file_tools.h"
#include "fat_dir_table.h"
#include "fat_file.h"

#define SIZE_OF_SECTOR                 512  //  MyFAt.sector_size
#define SIZE_OF_DIRECTORY_ENTRY        32

int main(void)
{
    char * pathname={"/App/.\0"};
    char * pathname2={"/This_is_a_loooooooooooooooooooooonnnnnnnnnnnnnnnnnnnnnggggggggggggggggggggggg_Dirrrrrrrrrrrrreeeeeeeeeeeeeeeeeecccccccccccccccttttttoooooorrrrrrrrryyyy/bob.txt\0"};
    char * pathname3={"WTF\0"};

    int fd=-1;

   fat_init(&MyFAt);
   print_fat(&MyFAt);
   
   printf("je recherche %s \n\n",pathname); 
   fd= g_open(pathname,1);
   COLOR_ON;
   printf(" fd= %d \n",fd);
   printf(" clus= %d \n",File_Descriptor[fd].first_cluster_num);
   COLOR_OFF;
   
   printf("je recherche %s \n\n",pathname2); 
   fd= g_open(pathname2,1);
   COLOR_ON;
   printf(" fd= %d \n",fd);
   printf(" clus= %d \n",File_Descriptor[fd].first_cluster_num);
   COLOR_OFF;

   printf("je recherche %s \n\n",pathname3); 
   fd= g_open(pathname3,1);
   COLOR_ON;
   printf(" fd= %d \n",fd);
   printf(" clus= %d \n",File_Descriptor[fd].first_cluster_num);
   COLOR_OFF;
   
   return 0;
}

