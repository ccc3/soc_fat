OBJ = fat_file.o  fat_file_tools.o  fat_tools.o  test.o utils.o 
SOURCE = fat_file.c  fat_file_tools.c  fat_tools.c  test.c utils.c
C_FLAGS = -Wall -g

all :  test

test : $(OBJ)
	gcc -o $@ $(OBJ)

%.o : %.c
	gcc -o $@ -c $< $(C_FLAGS)

clean : 
	rm *.o test

