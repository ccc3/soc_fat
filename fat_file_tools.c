
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <assert.h>

#include "utils.h"
#include "fat_fs_spec.h"
#include "fat_tools.h"
#include "fat_dir_table.h"
#include "fat_file_tools.h"


#define SIZE_OF_SECTOR                 512  //  MyFAt.sector_size
#define SIZE_OF_DIRECTORY_ENTRY        32
#define SIZE_OF_NAME_LONG              13

unsigned int lba_to_cluster(unsigned int  lba)                   //ok
{
   unsigned int clus;
   clus = ((lba - MyFAt.data_start)/ MyFAt.cluster_size ) + MyFAt.root_cluster;
   return clus;
}

unsigned int lba_to_block(unsigned lba)
{
   return lba/MyFAt.sector_size;
}

unsigned int cluster_to_lba(unsigned int cluster_number)       //ok
{
   return (unsigned int)(MyFAt.data_start +(MyFAt.sector_size * MyFAt.sector_per_cluster)*(cluster_number-MyFAt.root_cluster));
}


int get_next_cluster(unsigned int cluster_number)
{
   int next=0;
   FILE *file;
   unsigned char buffer[513]={'\0'};
   unsigned int page_table;
   if(( page_table = ( cluster_number / SIZE_CLUSTER_BUF )+ 1 ) == MyFAt.fat_table_page ) 
   {
      next = MyFAt.fat_table[cluster_number % SIZE_CLUSTER_BUF ];
   }else
   {
      MyFAt.fat_table_page=page_table;

      if(( file = fopen(HD_FAT,"r")) == NULL )
      {
         perror("open");
         exit(-1);
      }

      /******************todo****************************/
      fseek(file, ( MyFAt.fat_start +(int)(( MyFAt.fat_table_page-1) * SIZE_CLUSTER_BUF ) ) ,SEEK_SET);
      fread(buffer, 1, SIZE_CLUSTER_BUF , file);
      /**************************************************/
      short unsigned i=0;
      short unsigned off=0;
      while(i<SIZE_CLUSTER_BUF)
      {

         MyFAt.fat_table[i]= read_entry(off, CLUSTER_SIZE, buffer, 1);
         i++;
         off+=4;      
      }
      fclose(file);
      next = MyFAt.fat_table[cluster_number % SIZE_CLUSTER_BUF ];
   }
   return next=(next>=0x0ffffff8)?(-1):(next);
}


unsigned int read_cluster(unsigned char * short_buf)                 //ok
{
   unsigned int cluster_number=0;
   cluster_number = read_entry(DIR_FST_CLUS_HI,short_buf,1);
   cluster_number <<= get_length(DIR_FST_CLUS_HI) ;
   cluster_number = cluster_number | read_entry(DIR_FST_CLUS_LO,short_buf,1);
   return cluster_number;
}  
/******************************sfn*******************************/
unsigned char to_upper(unsigned char c)
{

   if (c >= 'a' && c <= 'z')
      return (c - ('a' - 'A'));
   else
      return c;
}

unsigned char illegal_short(unsigned char c)
{
   const unsigned char illegal_char [] =";+=[]’,\"*\\<>/?:|\0";
   short i=0;
   while (illegal_char[i]!='\0')
   {
      if (c == illegal_char[i])
         return '_';
      i++;
   }
   return c;
}

int is_short(unsigned char * string, unsigned char * sc_string)
{
   int s_size = 0;
   int dot_ext = 0;
   int name_len = 0;
   int ext_len = -1;
   int i = 0;
   int sc_i = 0;
   unsigned char ch;
   if(string[0] == '.' && string[1] == '\0')
   {
      sc_string[0] = '.';
      return 1;
   }

   if(string[0] == '.' && string[1] == '.' && string[2] == '\0')
   {
      sc_string[0] = '.';
      sc_string[1] = '.';
      return 1;
   }

   sc_string[11] = '\0';

   while (string[s_size] != '\0')
   {
      if (string[s_size] == '.')
      {
         dot_ext = s_size;
         ext_len = -1;
      }
      ext_len++;
      s_size++;
   }
   if (dot_ext != 0)
      name_len = (s_size - 1) - ext_len;
   else
      name_len = s_size;

   if (s_size > 12 || ext_len > 3 || (dot_ext == 0 && s_size > 8) || name_len > 8)
      return 0;

   if (dot_ext != 0)
   {
      while (i != ext_len)
      {
         ch = to_upper(string[dot_ext + 1 + i]);
         ch = illegal_short(ch);
         sc_string[8+i] = ch;
         i++;
      } 
   }
   i = 0;
   sc_i = 0;
   while (i!= name_len)
   {
      ch = to_upper(string[i]);
      ch = illegal_short(ch);
      if (ch != '.')
         sc_string[sc_i++] = ch;
      i++;
   }
   return 1;
}
/****************************************************************/

int read_block (unsigned char * block,unsigned char * buf,unsigned int size_elem)           
{

   unsigned int off=0;
   assert(size_elem <= 512 && size_elem >= 32 );
   while ( off<size_elem)
   {
      buf[off]=block[off];
      off++;
   }
   return off;
}

int fat_strcmp(unsigned char* filename ,unsigned char *entry_name)                      //ok 
{
   short int i=0; 
   while(entry_name[i]!='\0')
   {
      if(entry_name[i] != filename[i])
         return 1;
      i++;
   }
   return 0;
}


int get_name_from_short(unsigned char * dir_entry,unsigned char * entry_name)
{
   short int i=0;
   while(i < get_length(DIR_NAME))
   {
      entry_name[i]=dir_entry[i];
      i++;
   }

   entry_name[i]='\0';
   return i;
}

int get_name_from_long(unsigned char *dir_entry, unsigned char *entry_name)
{
   short int entry_name_off=0;
   unsigned int offset=get_length(LDIR_ORD);
   short int j=0;
   unsigned char eof=0;

   while(offset !=SIZE_OF_DIRECTORY_ENTRY  && !eof )
   {
      while(j != get_length(LDIR_NAME_1))
      {

         if(dir_entry[offset] == 0x00 || dir_entry[offset] ==  0xFF )
         {
            eof=1;
            break;
         }
         entry_name[entry_name_off]=dir_entry[offset];
         offset+=2;
         j+=2;
         entry_name_off++;
      }
      if (1 == eof)
         break;

      offset += (get_length(LDIR_ATTR) + get_length(LDIR_TYPE) + get_length(LDIR_CHK_SUM)) ;
      j=0;
      while(j != get_length(LDIR_NAME_2))
      {
         if(dir_entry[offset]==0x00 || dir_entry[offset] == 0xFF )
         {
            eof=1;
            break;
         }
         entry_name[entry_name_off]=dir_entry[offset];
         offset+=2;
         j+=2;
         entry_name_off++;
      }
      if (1 == eof)
         break;

      offset += get_length(LDIR_FST_CLUS_HI);
      j=0;
      while(j != get_length(LDIR_NAME_3))
      {
         if(dir_entry[offset]==0x00 || dir_entry[offset] == 0xFF )
         {
            eof=1;
            break;
         }
         entry_name[entry_name_off]=dir_entry[offset];
         offset+=2;
         j+=2;
         entry_name_off++;
      }
      if (1 == eof)
         break;
   }
   entry_name[entry_name_off]='\0';
   return entry_name_off;
}


int read_table_entry(unsigned int cluster_number,unsigned char * filename)
{
   FILE * file;
   unsigned char block[513]          = {'\0'};
   unsigned char dir_entry[33]       = {'\0'};
   unsigned char name_entry[14]      = {'\0'}; 
   unsigned char sfn[12]             = {[0 ... 10] = ' ', '\0'}; 
   unsigned char is_sfn              = is_short( filename, sfn);
   int           off                 = 0;
   unsigned int   cluster            = cluster_number;
   unsigned int  size_elem           = 0;
   unsigned int  real_offset         = cluster_to_lba(cluster_number); //lba_to_block(real_offset)
   unsigned int  dir_attr            = 0;
   unsigned int  dir_ord             = 0;
   unsigned char file_found          = 0;
   unsigned short  tmp               = 0;

   if(( file = fopen(HD_FAT,"r")) == NULL )
   {
      perror("open");
      exit(-1);
   }


   fseek( file, real_offset , SEEK_SET);
   fread(block, 1, MyFAt.sector_size, file);
   /**************************************************/

   while( 1 )
   {
      /******************** pour block*******************************/
      if (off >= MyFAt.sector_size)              //j'ai lu tt le buf
      {
         printf(" if  End block \n");
         real_offset += off;
         cluster=lba_to_cluster(real_offset);
         if(cluster !=cluster_number)           //fin du cluster
         {
            cluster = get_next_cluster(cluster_number);
            if(cluster ==-1)
            {

               printf("file not found\n");
               fclose(file);
               return -1;
            }
            cluster_number=cluster;
            real_offset=cluster_to_lba(cluster_number);
         }
         /*********************IOC_READ*******************/
         fseek( file, real_offset , SEEK_SET);
         fread(block, 1, MyFAt.sector_size, file);
         /********************IOC_READ*******************/   

         off = off % MyFAt.sector_size;
      }
      /****************************************************************/
      if (!file_found)
      { 
         dir_attr = read_entry(DIR_ATTR, block + off, 0);   
         dir_ord  = read_entry(DIR_ORD, block + off, 0);

         if ( 1 == is_sfn )
         {
            // long
            if ( (dir_attr == ATTR_LONG_NAME_MASK) &&( dir_ord != 0xE5) && ( dir_ord != 0x00 ))
            {
               size_elem  = dir_ord;
               size_elem  = size_elem & 0xf;
               size_elem *= SIZE_OF_DIRECTORY_ENTRY;
               off       += size_elem;
               continue;
               //short
            }else if ( (dir_attr != ATTR_LONG_NAME_MASK) && (dir_ord != 0xE5) && ( dir_ord != 0x00 ))
            {
               //je lis une short entry
               off += read_block( block+off, dir_entry, SIZE_OF_DIRECTORY_ENTRY);
            }else
            {
               if(dir_ord == 0x00)
               {
                  printf("file not found\n");
                  fclose(file);
                  return -1;
               }
               printf("else  free entry\n");
               off+=SIZE_OF_DIRECTORY_ENTRY;
               continue;
            }
         }else
         {
            if ( (dir_attr == ATTR_LONG_NAME_MASK) &&( dir_ord != 0xE5) && ( dir_ord != 0x00 ))
            {
               //je lis un long
               off += read_block( block+off, dir_entry, SIZE_OF_DIRECTORY_ENTRY);

            }//short
            else if ( (dir_attr != ATTR_LONG_NAME_MASK) && (dir_ord != 0xE5) && ( dir_ord != 0x00 ))
            {

               off += SIZE_OF_DIRECTORY_ENTRY;
               continue;
            }else
            {
               if(dir_ord == 0x00)
               {
                  printf("file not found\n");
                  fclose(file);
                  return -1;
               }
               off+=SIZE_OF_DIRECTORY_ENTRY;
               continue;
            }

         }
         //test de l'entre
         if( 1 == is_sfn)
         {
            get_name_from_short(dir_entry,name_entry);
            if ( 0 == fat_strcmp(sfn, name_entry) )
            {
               return read_cluster(dir_entry);
            }
         }
         else
         {
            tmp =  dir_ord & 0xf ;
            get_name_from_long(dir_entry,name_entry);
            if( 0 ==  fat_strcmp( filename +  ( ( tmp -1 ) * SIZE_OF_NAME_LONG ) , name_entry))
            {
               if(  1 == (dir_ord & 0xf) )
               {
                  file_found = 1;
                  continue;
               }
               continue;
            }
            else
            {
               off += tmp * SIZE_OF_DIRECTORY_ENTRY;
            }
         }
      } 
      else
      {
         off += read_block( block+off, dir_entry, SIZE_OF_DIRECTORY_ENTRY);
         return read_cluster(dir_entry);
      }
   }
   return -1;

}
